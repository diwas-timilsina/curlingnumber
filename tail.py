# Diwas and Tony 
# Generate tail sequence
 
import sys
import math 

def findCurl(seq,left_most):
    """find the curling number of the sequence"""
    best = 1
    for i in range(len(seq)-1,math.floor(len(seq)/2)-1,-1):        
        yPart = seq[i:]
        count = 1
        j = i
        while j > 0 and (yPart.__eq__(seq[j-len(yPart):j])):
            count+=1
            j -= len(yPart)
            if j<=left_most: left_most = j
        if count >= best:
            best = count
            
    return (best,left_most)


def generateTail(seq,left_most):
    """generate the tail sequence"""
    (curl,left_most_index) = findCurl(seq,left_most)
    if left_most_index >= left_most: left_most_index = left_most
  
    if curl == 1:
        return ((seq+[curl]),left_most_index)
    else:
        return generateTail((seq + [curl]),left_most_index)


def startSeq(n):
    """generates all starting sequences of length n"""
    sequences = []
    for i in range(0, 2<<(n-1)):
        seq = []
        for x in range(0,n):
            if i&1:
                seq.append(3)
            else:
                seq.append(2)
            i >>= 1
        sequences.append(seq)

    return sequences


if __name__ == '__main__':
    
    if len(sys.argv) < 2: 
        print("Eroor: need the length of the sequence")
        exit()
        
    n = int(sys.argv[1])
    
    test = startSeq(n)
    best_list = [[([1],[1],100)]]
    
    for i in range(len(test)):
        l,r = generateTail(test[i],len(test[i])-1)
        if len(l) > len(best_list[0][0]):
            best_list = []
            best_list.append((l,r,test[i]))
        elif len(l) == len(best_list[0][0]):
            best_list.append((l,r,test[i]))
            
    for itm in best_list: print ('\nSeed: {} \nLeft Most Index: {} \nTail: {}'.format(itm[2],itm[1],itm[0]))
    
    #print (generateTail([2,2,2,3,2,3,2],6))

